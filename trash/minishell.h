/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 11:08:20 by yaitalla          #+#    #+#             */
/*   Updated: 2015/01/30 11:28:54 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"
# include <dirent.h>
# include <sys/wait.h>
# include <signal.h>

typedef struct			s_sh
{
	char				*name;
	char				*path;
	int					time;
	struct s_sh			*next;
}						t_sh;

typedef struct			s_shell
{
	char				**env;
	char				*home;
	char				*path;
	char				*pwd;
	char				*old_pwd;
	t_sh				*sh;
	char				*cmd;
	char				*builtin[6];
}						t_shell;

void					ls_check(char **path);
void					get_stat(int i);
int						check_cmd(char *cmd);
void					cmd_split(t_shell *shell);
void					init_shell(t_shell **shell, char **environ);
void					ft_sh(t_shell **shell);
void					push(t_sh **sh, char *name, char *path);
t_sh					*new_h(char *name, char *path);
void					ft_shell(t_shell *shell);
char					*get_path(char *arg, t_sh *sh);
int						is_built(char *arg, t_shell *shell);
void					ft_fork(char *path, char **arg, t_shell *shell);
void					built(char **arg, t_shell *shell);
void					ft_cd(t_shell **shell, char **arg);
void					ft_env(t_shell *shell);
void					ft_direct(t_shell **shell, char *path, char *arg);
void					save(t_shell **shell);
void					ft_setenv(char *name, char *content, t_shell **shell);
void					new_env(t_shell **shell, char *str);
void					free_env(char ***env);
void					free_hash(t_sh **sh);
void					free_built(char **builtin);
void					ft_exit(t_shell **shell, char ***arg);
void					ft_checkenv(char **arg, t_shell **shell, int i);
void					ft_unsetenv(char **arg, t_shell **shell);
void					delete_env(t_shell **shell, char **arg, int i);

#endif
