/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 11:05:20 by yaitalla          #+#    #+#             */
/*   Updated: 2015/01/30 07:16:31 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void				init_shell(t_shell **shell, char **env)
{
	int					i;
	char				**temp;

	i = 0;
	(*shell)->env = (char **)malloc(sizeof(char *) * (ft_tablen(env) + 1));
	while (env[i])
	{
		(*shell)->env[i] = ft_strdup(env[i]);
		i++;
	}
	(*shell)->env[i] = NULL;
	i = 0;
	temp = (*shell)->env;
	while (temp[i])
	{
		if (ft_strncmp(temp[i], "PATH=", 5) == 0)
			(*shell)->path = ft_strdup(&(temp[i][5]));
		if (ft_strncmp(temp[i], "HOME=", 5) == 0)
			(*shell)->home = ft_strdup(&(temp[i][5]));
		if (ft_strncmp(temp[i], "PWD=", 4) == 0)
			(*shell)->pwd = ft_strdup(&(temp[i][4]));
		if (ft_strncmp(temp[i], "OLDPWD=", 7) == 0)
			(*shell)->old_pwd = ft_strdup(&(temp[i][7]));
		i++;
	}
}

void				built_init(t_shell **shell)
{
	(*shell)->builtin[0] = ft_strdup("cd");
	(*shell)->builtin[1] = ft_strdup("env");
	(*shell)->builtin[2] = ft_strdup("setenv");
	(*shell)->builtin[3] = ft_strdup("unsetenv");
	(*shell)->builtin[4] = ft_strdup("exit");
	(*shell)->builtin[5] = NULL;
}

void				ft_sh(t_shell **shell)
{
	char				**path_tab;
	DIR					*dir;
	struct dirent		*d;
	int					i;

	i = 0;
	path_tab = ft_strsplit((*shell)->path, ':');
	while (path_tab[i])
	{
		if ((dir = opendir(path_tab[i])) != NULL)
		{
			while ((d = readdir(dir)) != NULL)
			{
				if (d->d_name[0] != '.')
					push(&((*shell)->sh), d->d_name, path_tab[i]);
			}
			closedir(dir);
		}
		free(path_tab[i]);
		i++;
	}
	free(path_tab);
}

int					check_cmd(char *cmd)
{
	int					i;

	i = 0;
	while (cmd[i])
	{
		if (cmd[i] == ';')
			return (1);
		i++;
	}
	return (0);
}

int					main(void)
{
	t_shell				*shell;
	extern char			**environ;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->sh = NULL;
	init_shell(&shell, environ);
	built_init(&shell);
	ft_sh(&shell);
	while (42)
	{
		ft_putstr(shell->pwd);
		ft_putstr(" $> ");
		get_next_line(0, &(shell->cmd));
		if (check_cmd(shell->cmd))
			cmd_split(shell);
		else
		{
			if (shell->cmd[0])
				ft_shell(shell);
		}
		free(shell->cmd);
		shell->cmd = NULL;
	}
	return (0);
}
