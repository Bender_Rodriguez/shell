
#include "minish3.h"

int			terminit(t_all *all)
{
	struct winsize	win;

	if (tgetent(NULL, getenv("TERM")) < 1)
		return (0);
	if (tcgetattr(0, &(all->term)) == -1)
		return (0);
	all->term.c_lflag &= ~(ICANON | ECHO);
	all->term.c_cc[VMIN] = 1;
	all->term.c_cc[VTIME] = 0;
	ioctl(0, TIOCGWINSZ, &win);
	all->lins = win.ws_row;
	all->cols = win.ws_col;
	if (tcsetattr(0, 0, &(all->term)) == -1)
		return (0);
	tputs(tgetstr("ti", NULL), 1, tputchar);
	tputs(tgetstr("vi", NULL), 1, tputchar);
	return (1);
}

int			termclose(t_all *all)
{
	all->term.c_lflag |= (ICANON | ECHO);
	if (tcsetattr(0, 0, &(all->term)) == -1)
		return (0);
	tputs(tgetstr("te", NULL), 1, tputchar);
	tputs(tgetstr("ve", NULL), 1, tputchar);
	return (1);
}
/*
static void	go(t_all *all, char **av)
{
	screen_clear();
	fill_list(av, all);
	display(all->llst, all->color);
	size_ok(all);
	reset(0, all);
	while (1)
	{
		if (!key_ok(all))
			return ;
	}
}

void		go_down(t_all *all)
{
	t_tab		*lst;

	lst = all->llst;
	while (lst->current != 1)
		lst = lst->next;
	lst->current = 0;
	lst->next->current = 1;
}

void		go_up(t_all *all)
{
	t_tab		*lst;

	lst = all->llst;
	while (lst->current != 1)
		lst = lst->next;
	lst->current = 0;
	lst->prev->current = 1;
}

int			key_ok(t_all *all)
{
	char		buf[3];

	ft_memset(buf, 0, 3);
	read(0, buf, 3);
	if (K_ECHAP)
		return (0);
	if (size_ok(all))
	{
		key_action(all, buf);
		if (K_DEL || K_BAKSP)
		{
			if (!go_del(all))
				return (0);
		}
		if (K_ENTER)
		{
			all->enter = 1;
			return (0);
		}
		screen_clear();
		display(all->llst, all->color);
	}
	return (1);
}

int			move_cursor(int col, int row)
{
	char *res;
	char *res2;

	if ((res = tgetstr(C_CURS_MOVE, NULL)) == NULL)
		put_error(ERR_CAPS, 0);
	if ((res2 = tgoto(res, col, row)) == NULL)
		put_error(ERR_TGOTO, 0);
	tputs(res2, 0, put_caps);
	return (1);
}
*/
