/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/16 18:27:14 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 22:04:56 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_trial		*reset(int i, t_trial *trial)
{
	static t_trial	*temp = NULL;

	if (!i)
		temp = trial;
	return (temp);
}

int			terminit(t_shell *shell)
{
	struct winsize	win;

	if (tgetent(NULL, getenv("TERM")) < 1)
		return (0);
	if (tcgetattr(0, &(shell->trial.term)) == -1)
		return (0);
	shell->trial.term.c_lflag &= ~(ICANON);
	shell->trial.term.c_cc[VMIN] = 1;
	shell->trial.term.c_cc[VTIME] = 4;
	ioctl(0, TIOCGWINSZ, &win);
	shell->trial.lins = win.ws_row;
	shell->trial.cols = win.ws_col;
	if (tcsetattr(0, 0, &(shell->trial.term)) == -1)
		return (0);
	tputs(tgetstr("ti", NULL), 1, tputchar);
	//tputs(tgetstr("ve", NULL), 1, tputchar);
	return (1);
}

int			termclose(t_shell *shell)
{
	shell->trial.term.c_lflag |= (ICANON);
	if (tcsetattr(0, 0, &(shell->trial.term)) == -1)
		return (0);
	tputs(tgetstr("te", NULL), 1, tputchar);
	tputs(tgetstr("ve", NULL), 1, tputchar);
	return (1);
}

int			tputchar(int c)
{
	write(2, &c, 1);
	return (0);
}
