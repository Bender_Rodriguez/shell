/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 09:16:07 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 22:04:27 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H

# include "libft.h"
# include "colors.h"
# include <dirent.h>
# include <sys/wait.h>
# include <sys/ioctl.h>
# include <signal.h>
# include <termcap.h>
# include <termios.h>
# include <curses.h>
# include <term.h>

# define IN			0
# define OUT		1
# define LEFT		0
# define RIGHT		1
# define X			0
# define Y			1
# define K_UP		(buf[0] == 27 && buf[1] == 91 && buf[2] == 65)
# define K_DOWN		(buf[0] == 27 && buf[1] == 91 && buf[2] == 66)
# define K_RIGHT	(buf[0] == 27 && buf[1] == 91 && buf[2] == 67)
# define K_LEFT		(buf[0] == 27 && buf[1] == 91 && buf[2] == 68)
# define K_END		(buf[0] == 27 && buf[1] == 91 && buf[2] == 70)
# define K_HOME		(buf[0] == 27 && buf[1] == 91 && buf[2] == 72)
# define K_ENTER	(buf[0] == 10 && !buf[1] && !buf[2])
# define K_ECHAP	(buf[0] == 27 && !buf[1] && !buf[2])
# define K_SPACE	(buf[0] == 32 && !buf[1] && !buf[2])
# define K_BAKSP	(buf[0] == 127 && !buf[1] && !buf[2])
# define K_DEL		(buf[0] == 27 && buf[1] == 91 && buf[2] == 51)
# define ARROW		(K_LEFT || K_RIGHT || K_DOWN || K_UP)
# define SPECIAL	(K_BAKSP || K_ECHAP || K_DEL || K_HOME || K_END)
# define BAKSP		cursor[2]

typedef struct			s_sh
{
	char				*name;
	char				*path;
	int					time;
	struct s_sh			*next;
}						t_sh;

typedef struct			s_pipe
{
	char				**cmds;
	char				**avs;
	char				**avs2;
	char				*path[2];
}						t_pipe;

typedef struct			s_trial
{
	struct termios		term;
	int					size_ok;
	int					cols;
	int					lins;
	int					width;
	int					heigh;
}						t_trial;

typedef struct			s_shell
{
	t_pipe				pipe;
	t_trial				trial;
	int					fd[2];
	int					cursor[3];
	int					baksp;
	int					pip;
	int					redir;
	int					pvirg;
	char				**env;
	char				**hist;
	char				*home;
	char				*path;
	char				*pwd;
	char				*old_pwd;
	t_sh				*hash;
	char				*cmd;
	char				*builtin[8];
}						t_shell;

int						terminit(t_shell *shell);
int						termclose(t_shell *shell);
t_trial					*reset(int i, t_trial *trial);
int						tputchar(int c);
void					prompt(t_shell *shell);
void					init_shell(t_shell **shell, char **environ);
void					init_builtin(t_shell **shell);
void					ft_hash(t_shell **shell);
void					push(t_sh **sh, char *name, char *path);
void					get_stat(int i);
void					init_all(t_shell *shell, char **environ);
t_sh					*new_h(char *name, char *path);
void					ft_direct(t_shell **shell, char *path, char *arg);
void					save(t_shell **shell);
void					ft_unsetenv(char **arg, t_shell **shell);
void					ft_setenv(char *name, char *content, t_shell **shell);
void					free_env(char ***env);
void					ft_env(t_shell *shell);
void					ft_exit(t_shell **shell, char ***arg);
void					ft_checkenv(char **arg, t_shell **shell, int i);
void					ft_shell(t_shell *shell);
void					ft_fork(char *path, char **arg, t_shell *shell);
int						is_built(char *arg, t_shell *shell);
void					built(char **arg, t_shell *shell);
char					*get_path(char *arg, t_sh *sh);
void					ls_check(char **path);
void					special_key(char buf[3], t_shell *shell);
void					check_arrow(char buf[3], int cursor[3]);
void					backspace(t_shell *shell);
/*
void					reset_pipe(t_shell *shell);
void					save(t_shell **shell);
void					ft_setenv(char *name, char *cont, t_shell **shell);
void					free_env(char ***env);
void					ft_direct(t_shell **shell, char *path, char *arg);
int						check_pipe(t_shell *shell);
int						check_cmd(char *cmd, t_shell *shell);
void					cmd_split(t_shell *shell);
void					ft_cd(t_shell **shell, char **arg);
void					new_env(t_shell **shell, char *str);
void					free_hash(t_sh **sh);
void					free_built(char **builtin);
void					delete_env(t_shell **shell, char **arg, int i);
*/

#endif
