/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 09:53:36 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/18 10:00:39 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void				new_env(t_shell **shell, char *str)
{
	char				**new;
	int					i;

	i = 0;
	new = (char **)malloc(sizeof(char *) * (ft_tablen((*shell)->env) + 2));
	while ((*shell)->env[i])
	{
		new[i] = ft_strdup((*shell)->env[i]);
		i++;
	}
	new[i++] = str;
	new[i] = 0;
	free_env(&((*shell)->env));
	(*shell)->env = new;
}

void				delete_env(t_shell **shell, char **arg, int i)
{
	int					j;
	int					k;
	char				**new;

	new = (char **)malloc(sizeof(char *) * ft_tablen((*shell)->env));
	j = 0;
	k = 0;
	while ((*shell)->env[j])
	{
		if (ft_strncmp((*shell)->env[j], arg[i],
					e_len((*shell)->env[j])) != 0 ||
				ft_strlen(arg[i]) != e_len((*shell)->env[j]))
			new[k++] = ft_strdup((*shell)->env[j]);
		j++;
	}
	new[k] = 0;
	free_env(&((*shell)->env));
	(*shell)->env = new;
}

void				ft_setenv(char *name, char *cont, t_shell **shell)
{
	char				*str;
	int					i;
	int					j;

	str = (char *)ft_memalloc(ft_strlen(name) + ft_strlen(cont) + 2);
	ft_strcat(str, name);
	ft_strcat(str, "=");
	ft_strcat(str, cont);
	i = 0;
	j = 1;
	while ((*shell)->env[i] && j)
	{
		if (ft_strncmp((*shell)->env[i], name, e_len((*shell)->env[i])) == 0 &&
					ft_strlen(name) == e_len((*shell)->env[i]))
		{
			free((*shell)->env[i]);
			(*shell)->env[i] = str;
			j = 0;
		}
		i++;
	}
	if (j)
		new_env(shell, str);
}

void				ft_unsetenv(char **arg, t_shell **shell)
{
	int					i;
	int					j;

	i = 0;
	while (i < ft_tablen(arg))
	{
		j = 0;
		while ((*shell)->env[j])
		{
			if (ft_strncmp((*shell)->env[j], arg[i],
							e_len((*shell)->env[j])) == 0 &&
				ft_strlen(arg[i]) == e_len((*shell)->env[j]))
			{
				delete_env(shell, arg, i);
				break ;
			}
			j++;
		}
		i++;
	}
}

void				ft_checkenv(char **arg, t_shell **shell, int i)
{
	if (ft_tablen(arg) == 1)
		ft_env(*shell);
	else if (ft_tablen(arg) > 3 && i == 1)
	{
		putcolor(arg[0], BOLD_RED, 2, 0);
		putcolor(": Too many arguments.", RED, 2, 1);
	}
	else
	{
		if (i == 1)
		{
			if (ft_tablen(arg) == 2)
				ft_setenv(arg[1], "\0", shell);
			else
				ft_setenv(arg[1], arg[2], shell);
		}
		if (i == 2)
			ft_unsetenv(arg + 1, shell);
	}
}
