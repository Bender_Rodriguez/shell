/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/16 19:01:52 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 21:16:07 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void				init_all(t_shell *shell, char **environ)
{
	init_shell(&shell, environ);
	init_builtin(&shell);
	ft_hash(&shell);
	terminit(shell);
}

void				reset_pipe(t_shell *shell)
{
	shell->pip = 0;
	shell->redir = 0;
	shell->pvirg = 0;
	shell->hist = (char **)malloc(sizeof(char *) * 2);
	shell->hist[0] = "You have reached the end of history.\n";
	shell->cursor[X] = 9;
	shell->cursor[Y] = 0;
	shell->BAKSP = 0;
}

void				init_shell(t_shell **shell, char **env)
{
	int					i;
	char				**temp;

	i = 0;
	(*shell)->env = (char **)malloc(sizeof(char *) * (ft_tablen(env) + 1));
	while (env[i])
	{
		(*shell)->env[i] = ft_strdup(env[i]);
		i++;
	}
	(*shell)->env[i] = NULL;
	i = 0;
	temp = (*shell)->env;
	while (temp[i])
	{
		if (ft_strncmp(temp[i], "PATH=", 5) == 0)
			(*shell)->path = ft_strdup(&(temp[i][5]));
		if (ft_strncmp(temp[i], "HOME=", 5) == 0)
			(*shell)->home = ft_strdup(&(temp[i][5]));
		if (ft_strncmp(temp[i], "PWD=", 4) == 0)
			(*shell)->pwd = ft_strdup(&(temp[i][4]));
		if (ft_strncmp(temp[i], "OLDPWD=", 7) == 0)
			(*shell)->old_pwd = ft_strdup(&(temp[i][7]));
		i++;
	}
}

void				init_builtin(t_shell **shell)
{
	(*shell)->builtin[0] = ft_strdup("cd");
	(*shell)->builtin[1] = ft_strdup("env");
	(*shell)->builtin[2] = ft_strdup("setenv");
	(*shell)->builtin[3] = ft_strdup("unsetenv");
	(*shell)->builtin[4] = ft_strdup("exit");
	(*shell)->builtin[5] = ft_strdup("hist");
	(*shell)->builtin[6] = ft_strdup("hash");
	(*shell)->builtin[7] = NULL;
	reset_pipe(*shell);
}

void				ft_hash(t_shell **shell)
{
	char				**path_tab;
	DIR					*dir;
	struct dirent		*d;
	int					i;

	i = 0;
	path_tab = ft_strsplit((*shell)->path, ':');
	while (path_tab[i])
	{
		if ((dir = opendir(path_tab[i])) != NULL)
		{
			while ((d = readdir(dir)) != NULL)
			{
				if (d->d_name[0] != '.')
					push(&((*shell)->hash), d->d_name, path_tab[i]);
			}
			closedir(dir);
		}
		free(path_tab[i]);
		i++;
	}
	free(path_tab);
}
