/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 16:19:39 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 22:05:01 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

static int		check_sav(char **save, char **line)
{
	char	*temp;

	if ((temp = ft_strchr(*save, '\n')))
	{
		*temp = '\0';
		*line = ft_strdup(*save);
		ft_strcpy(*save, temp + 1);
		return (1);
	}
	return (0);
}

static int		check_rad(char *buf, char **save, char **line)
{
	char	*temp;

	if (!(*save))
		*save = "";
	if ((temp = ft_strchr(buf, '\n')))
	{
		*temp = '\0';
		*line = ft_strjoin(*save, buf);
		*save = ft_strdup(temp + 1);
		temp = NULL;
		return (1);
	}
	return (0);
}

static void			check_key(char buf[3], t_shell *shell)
{
	if (!K_BAKSP)
		shell->cursor[X]++;
	if (ARROW || SPECIAL)
		special_key(buf, shell);
}

static int			check_line(int const fd, char **line, t_shell *shell)
{
	static char		*save = NULL;
	char			*buf;
	int				r;

	if (save)
		if (check_sav(&save, line))
			return (1);
	buf = ft_strnew(BUFF_SIZE);
	while ((r = read(fd, buf, BUFF_SIZE)) > 0)
	{
		check_key(buf, shell);
		buf[r] = '\0';
		if (check_rad(buf, &save, line))
			return (1);
		save = ft_strjoin(save, buf);
	}
	if (r == -1)
		return (-1);
	if (save == NULL)
		return (0);
	*line = ft_strdup(save);
	ft_strdel(&save);
	return (1);
}

void		prompt(t_shell *shell)
{
	if (check_line(0, &(shell->cmd), shell) == 0)
		exit(0);
	shell->cursor[Y]++;
	shell->cursor[X] = 9;
	if (shell->cmd)
	{
		if (shell->BAKSP)
			backspace(shell);
		//shell->hist = tabtabplus(shell->hist, shell->cmd);
		ft_shell(shell);
	}
	free(shell->cmd);
	shell->cmd = NULL;
}
