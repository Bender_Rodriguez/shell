/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 09:49:43 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 12:10:57 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void				ft_env(t_shell *shell)
{
	int					i;

	i = 0;
	while (shell->env[i])
	{
		putcolor(shell->env[i], BROWN, 1, 1);
		i++;
	}
}

void				ft_cd(t_shell **shell, char **arg)
{
	char				*path;

	if (ft_tablen(arg) == 1)
		path = ft_strdup((*shell)->home);
	else if (ft_strcmp(arg[1], "-") == 0)
		path = ft_strdup((*shell)->old_pwd);
	else
	{
		if (arg[1][0] == '/')
			path = ft_strdup(arg[1]);
		else
		{
			path = (char *)ft_memalloc(ft_strlen((*shell)->pwd) +
				ft_strlen(arg[1]) + 2);
			ft_strcat(path, (*shell)->pwd);
			ft_strcat(path, "/");
			ft_strcat(path, arg[1]);
		}
	}
	ft_direct(shell, path, arg[1]);
	free(path);
}

void				built(char **arg, t_shell *shell)
{
	if (ft_strcmp(arg[0], "exit") == 0)
		ft_exit(&shell, &arg);
	else if (ft_strcmp(arg[0], "cd") == 0)
		ft_cd(&shell, arg);
	else if (ft_strcmp(arg[0], "env") == 0)
		ft_env(shell);
	else if (ft_strcmp(arg[0], "setenv") == 0)
		ft_checkenv(arg, &shell, 1);
	else if (ft_strcmp(arg[0], "unsetenv") == 0)
		ft_checkenv(arg, &shell, 2);
	else if (ft_strcmp(arg[0], "hist") == 0)
		putcolor(shell->hist[0], MAGENTA, 1, 1);
}

void				ls_check(char **path)
{
	if (ft_strcmp(*path, "ls") == 0)
		*path = ft_strdup("/bin/ls");
}

int					is_built(char *arg, t_shell *shell)
{
	int					i;

	i = 0;
	while (shell->builtin[i])
	{
		if (ft_strcmp(arg, shell->builtin[i]) == 0)
			return (1);
		i++;
	}
	return (0);
}
