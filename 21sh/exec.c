/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/21 15:10:02 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 22:04:23 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void				ft_fork(char *path, char **arg, t_shell *shell)
{
	pid_t				pid;

	pid = fork();
	if (pid == 0)
		execve(path, arg, shell->env);
	if (pid > 0)
		wait(NULL);
}

void				ft_shell(t_shell *shell)
{
	char				**arg;
	char				*path;
	int					i;

	arg = ft_space_split(shell->cmd);
	if (arg && arg[0])
	{
		i = 0;
		if (is_built(arg[0], shell))
			built(arg, shell);
		else if ((path = get_path(arg[0], shell->hash)))
		{
			ls_check(&path);
			ft_fork(path, arg, shell);
		}
		else
		{
			putcolor("21_SH: command not found: ", BOLD_RED, 2, 0);
			putcolor(arg[0], BOLD_RED, 2, 1);
			putcolor(shell->cmd, BOLD_LIGHT_GREY, 1, 1);
			putcolor(ft_itoa(ft_strlen(shell->cmd)), BOLD_LIGHT_GREY, 1, 1);
			putcolor(ft_itoa(shell->BAKSP), BOLD_LIGHT_GREY, 1, 1);
		}
		while (i < ft_tablen(arg))
			free(arg[i++]);
		free(arg);
	}
}

char				*get_path(char *arg, t_sh *sh)
{
	if (access(arg, F_OK) == 0)
	{
		while (sh)
		{
			if (recurcmp(sh->path, arg) == 0)
				sh->time++;
			sh = sh->next;
		}
		return (arg);
	}
	else
	{
		while (sh)
		{
			if (recurcmp(sh->name, arg) == 0)
			{
				sh->time++;
				return (sh->path);
			}
			sh = sh->next;
		}
	}
	return (NULL);
}
