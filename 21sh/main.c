/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/09 18:20:15 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 22:04:25 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int			main(void)
{
	extern char			**environ;
	t_shell				*shell;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->hash = NULL;
	init_all(shell, environ);
	while (1)
	{
		if (shell->cmd)
		{
			putcolor(shell->cmd, BROWN, 1, 0);
			putcolor(" $> ", MAGENTA, 1, 0);
		}
		else
		{
			putcolor("21_SH", BROWN, 1, 0);
			putcolor(" $> ", MAGENTA, 1, 0);
		}
		prompt(shell);
	}
	termclose(shell);
	return (0);
}
