/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/21 16:09:58 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/22 22:04:54 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void			backspace(t_shell *shell)
{
    int				len;
    int				i;
    char			*temp;

    i = 0;
    len = ft_strlen(shell->cmd) - (shell->BAKSP * 2) + 1;
    putcolor("---------------------------", BOLD_CYAN, 1, 1);
    putcolor("         Backspace", BOLD_CYAN, 1, 1);
    putcolor("---------------------------", BOLD_CYAN, 1, 1);
    putcolor("> strlen(cmd_malloc_size) = ", BOLD_CYAN, 1, 0);
    putcolor(ft_itoa(ft_strlen(shell->cmd) - (shell->BAKSP * 2) + 1), BOLD_CYAN, 1, 1);
    putcolor("> len    : ok   ", BOLD_CYAN, 1, 0);
    putcolor(ft_itoa(len), BOLD_CYAN, 1, 1);
    if (len > 0)
    {
        temp = (char *)malloc(sizeof(char) * len);
        putcolor("> malloc : ok   sizeof(char*) * len", BOLD_CYAN, 1, 1);
        shell->BAKSP += shell->BAKSP;
        putcolor("> BAKSP  = ", BOLD_CYAN, 1, 0);
        putcolor(ft_itoa(shell->BAKSP), BOLD_CYAN, 1, 1);
        if (shell->cmd)
        {
            putcolor("> while shell->cmd", BOLD_CYAN, 1, 1);
            putcolor("> (shell->cmd =  :", BOLD_CYAN, 1, 1);
            putcolor(shell->cmd, BOLD_CYAN, 1, 1);
            while (shell->cmd[shell->BAKSP])
            {
                putcolor("> shell->BAKSP = ", BOLD_CYAN, 1,0);
                putcolor(ft_itoa(shell->BAKSP), BOLD_CYAN, 1,1);
                temp[i] = shell->cmd[shell->BAKSP];
                shell->BAKSP++;
                i++;
            }
            temp[i] = '\0';
        }
        free(shell->cmd);
        shell->cmd = ft_strdup(temp);
    }
    shell->BAKSP = 0;
}

void			check_arrow(char buf[3], int cursor[3])
{
    if (K_LEFT)
    {
        tputs(tgoto(tgetstr("cm", NULL), cursor[X] - 2, cursor[Y]), 1, tputchar);
        tputs(tgetstr("ce", NULL), 1, tputchar);
    }
    if (K_RIGHT)
    {
        tputs(tgoto(tgetstr("cm", NULL), cursor[X] + 1, cursor[Y]), 1, tputchar);
    }
    if (K_UP || K_DOWN)
        putcolor("Historic", BROWN, 1, 1);
}

static void		check_special(char buf[3], t_shell *shell)
{
    char			*sb;

    sb = (char *)malloc(sizeof(char) * 2048);
    if (K_BAKSP)
    {
        shell->BAKSP++;
        if (shell->cursor[X] > 9)
        {
            ft_putchar('\b');
        }
        ft_putchar('\b');
        ft_putchar('\b');
        tputs(tgetstr("ce", &sb), 1, tputchar);
        shell->cursor[X]--;
        /*
           putcolor("BAKSP", GREEN, 1, 0);
           tputs(tgetstr("dc", NULL), 1, tputchar);
         */
    }
    if (K_DEL)
    {
        putcolor("DELETE", GREEN, 1, 0);
        tputs(tgetstr("dc", NULL), 1, tputchar);
    }
}

void			special_key(char buf[3], t_shell *shell)
{
    if (ARROW)
        check_arrow(buf, shell->cursor);
    if (SPECIAL)
        check_special(buf, shell);
}
