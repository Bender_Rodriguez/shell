/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/09 18:20:15 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/09 18:37:17 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh.h"

int			main(void)
{
	extern char			**environ;
	t_shell				*shell;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->hash = NULL;
	init_all(shell, environ);
	while (1)
	{
		prompt(shell);
	}
	termclose(shell);
	return (0);
}
