/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/09 18:34:25 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/09 18:37:15 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH3_H
# define SH3_H

# include "libft.h"
# include "select.h"
# include "colors.h"
# include <dirent.h>
# include <sys/wait.h>
# include <sys/ioctl.h>
# include <signal.h>
# include <termcap.h>
# include <termios.h>
# include <curses.h>
# include <term.h>

# define IN			0
# define OUT		1
# define LEFT		0
# define RIGHT		1
# define K_UP		(buf[0] == 27 && buf[1] == 91 && buf[2] == 65)
# define K_DOWN		(buf[0] == 27 && buf[1] == 91 && buf[2] == 66)
# define K_ENTER	(buf[0] == 10 && !buf[1] && !buf[2])
# define K_ECHAP	(buf[0] == 27 && !buf[1] && !buf[2])
# define K_SPACE	(buf[0] == 32 && !buf[1] && !buf[2])
# define K_DEL		(buf[0] == 127 && !buf[1] && !buf[2])
# define K_BAKSP	(buf[0] == 27 && buf[1] == 91 && buf[2] == 51)

typedef struct			s_sh
{
	char				*name;
	char				*path;
	int					time;
	struct s_sh			*next;
}						t_sh;

typedef struct			s_pipe
{
	char				**cmds;
	char				**avs;
	char				**avs2;
	char				*path[2];
}						t_pipe;

typedef struct			s_shell
{
	t_pipe				pipe;
	t_trial				trial;
	int					fd[2];
	int					pip;
	int					redir;
	int					pvirg;
	char				**env;
	char				*home;
	char				*path;
	char				*pwd;
	char				*old_pwd;
	t_sh				*hash;
	char				*cmd;
	char				*builtin[8];
}						t_shell;

void					prompt(t_shell *shell);
void					init_shell(t_shell **shell, char **environ);
void					init_builtin(t_shell **shell);
void					ft_hash(t_shell **shell);
void					push(t_sh **sh, char *name, char *path);
void					reset_pipe(t_shell *shell);
int						terminit(t_shell *shell);
int						termclose(t_shell *shell);
void					save(t_shell **shell);
void					ft_setenv(char *name, char *cont, t_shell **shell);
void					free_env(char ***env);
void					ft_direct(t_shell **shell, char *path, char *arg);
void					ft_exit(t_shell **shell, char ***arg);
void					ft_checkenv(char **arg, t_shell **shell, int i);
void					ft_env(t_shell *shell);
void					init_all(t_shell *shell, char **environ);
/*
int						check_pipe(t_shell *shell);
void					ls_check(char **path);
void					get_stat(int i);
int						check_cmd(char *cmd, t_shell *shell);
void					cmd_split(t_shell *shell);
t_sh					*new_h(char *name, char *path);
void					ft_shell(t_shell *shell);
char					*get_path(char *arg, t_sh *sh);
int						is_built(char *arg, t_shell *shell);
void					ft_fork(char *path, char **arg, t_shell *shell);
void					built(char **arg, t_shell *shell);
void					ft_cd(t_shell **shell, char **arg);
void					ft_env(t_shell *shell);
void					ft_direct(t_shell **shell, char *path, char *arg);
void					save(t_shell **shell);
void					ft_setenv(char *name, char *content, t_shell **shell);
void					new_env(t_shell **shell, char *str);
void					free_env(char ***env);
void					free_hash(t_sh **sh);
void					free_built(char **builtin);
void					ft_exit(t_shell **shell, char ***arg);
void					ft_checkenv(char **arg, t_shell **shell, int i);
void					ft_unsetenv(char **arg, t_shell **shell);
void					delete_env(t_shell **shell, char **arg, int i);
*/

#endif
