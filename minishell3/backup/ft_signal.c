/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitshella <yaitshella@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/28 05:46:56 by yaitshella          #+#    #+#             */
/*   Updated: 2015/09/01 14:00:51 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

static void		canceller(void)
{
	t_shell		*shell;

	shell = NULL;
	shell = reset(1, shell);
	termclose(shell);
	exit(0);
}

static void		continuer(void)
{
	t_shell		*shell;

	shell = NULL;
	shell = reset(1, shell);
	shell->term.c_lflag &= ~(ICANON | ECHO);
	shell->term.c_cc[VMIN] = 1;
	shell->term.c_cc[VTIME] = 0;
	tcsetattr(0, 0, &(shell->term));
	tputs(tgetstr("ti", NULL), 1, tputchar);
	tputs(tgetstr("vi", NULL), 1, tputchar);
	signal(SIGTSTP, sig_catcher);
	resizer();
	size_ok(shell);
}

static void		stopper(void)
{
	t_shell		*shell;
	char		cp[2];

	shell = NULL;
	shell = reset(1, shell);
	cp[0] = shell->term.c_cc[VSUSP];
	cp[1] = 0;
	shell->term.c_lflag |= (ICANON | ECHO);
	signal(SIGTSTP, SIG_DFL);
	screen_clear();
	tcsetattr(0, 0, &(shell->term));
	tputs(tgetstr("te", NULL), 1, tputchar);
	tputs(tgetstr("ve", NULL), 1, tputchar);
	ioctl(0, TIOCSTI, cp);
}

void			sig_catcher(int i)
{
	if (i == SIGCONT)
		continuer();
	else if (i == SIGTSTP)
		stopper();
	else if (i == SIGWINCH)
		resizer();
	else
		canceller();
}

void				signaler(void)
{
	int				i;

	i = 1;
	while (i < 32)
	{
		signal(i, sig_catcher);
		i++;
	}
}
