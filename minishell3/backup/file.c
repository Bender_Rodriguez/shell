/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitshella <yaitshella@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 17:03:50 by yaitshella          #+#    #+#             */
/*   Updated: 2015/09/01 14:25:26 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

void		screen_clear(void)
{
	tputs(tgetstr("cl", NULL), 1, tputchar);
}

int			tputchar(int c)
{
	write(2, &c, 1);
	return (0);
}

t_shell		*reset(int i, t_shell *shell)
{
	static t_shell	*temp = NULL;

	if (!i)
		temp = shell;
	return (temp);
}

int			size_ok(t_shell *shell)
{
	if (shell->cols - 2 < shell->width || shell->lins - 2 < shell->lenght)
	{
		screen_clear();
		putcolor("The window is too smshell.", BOLD_RED, 2, 1);
		return (0);
	}
	else
	{
		screen_clear();
		ft_putendl(shell->pwd);
		//display(shell->llst, shell->color);
	}
	return (1);
}

void		signaler(void)
{
	int			i;

	i = 1;
	while (i < 32)
	{
		signal(i, sig_catcher);
		i++;
	}
}
