/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/29 04:44:42 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/01 14:25:18 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

void				init_shell(t_shell **shell, char **env)
{
	int					i;
	char				**temp;

	i = 0;
	(*shell)->env = (char **)malloc(sizeof(char *) * (ft_tablen(env) + 1));
	while (env[i])
	{
		(*shell)->env[i] = ft_strdup(env[i]);
		i++;
	}
	(*shell)->env[i] = NULL;
	i = 0;
	temp = (*shell)->env;
	while (temp[i])
	{
		if (ft_strncmp(temp[i], "PATH=", 5) == 0)
			(*shell)->path = ft_strdup(&(temp[i][5]));
		if (ft_strncmp(temp[i], "HOME=", 5) == 0)
			(*shell)->home = ft_strdup(&(temp[i][5]));
		if (ft_strncmp(temp[i], "PWD=", 4) == 0)
			(*shell)->pwd = ft_strdup(&(temp[i][4]));
		if (ft_strncmp(temp[i], "OLDPWD=", 7) == 0)
			(*shell)->old_pwd = ft_strdup(&(temp[i][7]));
		i++;
	}
}

static void			built_init(t_shell **shell)
{
	(*shell)->builtin[0] = ft_strdup("cd");
	(*shell)->builtin[1] = ft_strdup("env");
	(*shell)->builtin[2] = ft_strdup("setenv");
	(*shell)->builtin[3] = ft_strdup("unsetenv");
	(*shell)->builtin[4] = ft_strdup("hash");
	(*shell)->builtin[5] = ft_strdup("echo");
	(*shell)->builtin[6] = ft_strdup("exit");
	(*shell)->builtin[7] = NULL;
	(*shell)->pip = 0;
	(*shell)->redir = 0;
	(*shell)->pvirg = 0;
}

static void			ft_hash(t_shell **shell)
{
	char				**path_tab;
	DIR					*dir;
	struct dirent		*d;
	int					i;

	i = 0;
	path_tab = ft_strsplit((*shell)->path, ':');
	while (path_tab[i])
	{
		if ((dir = opendir(path_tab[i])) != NULL)
		{
			while ((d = readdir(dir)) != NULL)
			{
				if (d->d_name[0] != '.')
					push(&((*shell)->hash), d->d_name, path_tab[i]);
			}
			closedir(dir);
		}
		free(path_tab[i]);
		i++;
	}
	free(path_tab);
}

int					main(void)
{
	t_shell				*shell;
	extern char			**environ;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->hash = NULL;
	init_shell(&shell, environ);
	built_init(&shell);
	ft_hash(&shell);
	signaler();
	if (!terminit(shell))
		return (1);
	while (42)
	{
		putcolor(shell->pwd, BROWN, 1, 0);
		putcolor(" $> ", MAGENTA, 1, 0);
		if (get_next_line(0, &(shell->cmd)) == 0)
			exit (0);
		/*
		if (check_cmd(shell->cmd, shell))
			cmd_split(shell);
			*/
		else
		{
			if (shell->cmd[0])
				ft_shell(shell);
		}
		free(shell->cmd);
		shell->cmd = NULL;
	}
	if (!termclose(shell))
		return (1);
	return (0);
}
/*
int					check_cmd(char *cmd, t_shell *shell)
{
	int					i;

	i = 0;
	while (cmd[i])
	{
		if (cmd[i] == ';')
			shell->pvirg = 1;
		if (cmd[i] == '|')
			shell->pip++;
		if (cmd[i] == '<' || cmd[i] == '>')
			shell->redir++;
		i++;
	}
	if (shell->pvirg)
		return (1);
	return (0);
}
*/
