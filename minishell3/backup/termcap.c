
#include "sh3.h"

int			terminit(t_shell *shell)
{
	struct winsize	win;

	if (tgetent(NULL, getenv("TERM")) < 1)
		return (0);
	if (tcgetattr(0, &(shell->term)) == -1)
		return (0);
	shell->term.c_lflag &= ~(ICANON | ECHO);
	shell->term.c_cc[VMIN] = 1;
	shell->term.c_cc[VTIME] = 0;
	ioctl(0, TIOCGWINSZ, &win);
	shell->lins = win.ws_row;
	shell->cols = win.ws_col;
	if (tcsetattr(0, 0, &(shell->term)) == -1)
		return (0);
	tputs(tgetstr("ti", NULL), 1, tputchar);
	tputs(tgetstr("vi", NULL), 1, tputchar);
	return (1);
}

int			termclose(t_shell *shell)
{
	shell->term.c_lflag |= (ICANON | ECHO);
	if (tcsetattr(0, 0, &(shell->term)) == -1)
		return (0);
	tputs(tgetstr("te", NULL), 1, tputchar);
	tputs(tgetstr("ve", NULL), 1, tputchar);
	return (1);
}
/*
static void	go(t_shell *shell, char **av)
{
	screen_clear();
	fill_list(av, shell);
	display(shell->llst, shell->color);
	size_ok(shell);
	reset(0, shell);
	while (1)
	{
		if (!key_ok(shell))
			return ;
	}
}

void		go_down(t_shell *shell)
{
	t_tab		*lst;

	lst = shell->llst;
	while (lst->current != 1)
		lst = lst->next;
	lst->current = 0;
	lst->next->current = 1;
}

void		go_up(t_shell *shell)
{
	t_tab		*lst;

	lst = shell->llst;
	while (lst->current != 1)
		lst = lst->next;
	lst->current = 0;
	lst->prev->current = 1;
}

int			key_ok(t_shell *shell)
{
	char		buf[3];

	ft_memset(buf, 0, 3);
	read(0, buf, 3);
	if (K_ECHAP)
		return (0);
	if (size_ok(shell))
	{
		key_action(shell, buf);
		if (K_DEL || K_BAKSP)
		{
			if (!go_del(shell))
				return (0);
		}
		if (K_ENTER)
		{
			shell->enter = 1;
			return (0);
		}
		screen_clear();
		display(shell->llst, shell->color);
	}
	return (1);
}

int			move_cursor(int col, int row)
{
	char *res;
	char *res2;

	if ((res = tgetstr(C_CURS_MOVE, NULL)) == NULL)
		put_error(ERR_CAPS, 0);
	if ((res2 = tgoto(res, col, row)) == NULL)
		put_error(ERR_TGOTO, 0);
	tputs(res2, 0, put_caps);
	return (1);
}
*/
