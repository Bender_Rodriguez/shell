/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 13:32:24 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/01 13:38:06 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

const char			*g_signals[] = {
	"",
	"Hangup",
	"",
	"Quit",
	"Illegal instruction",
	"Trace/BPT trap",
	"Abort trap",
	"EMT trap",
	"Floating point exception",
	"Killed",
	"Bus error",
	"Segmentation fault",
	"Bad system call",
	"",
	"Alarm clock",
	"Terminated",
	"",
	"Stopped",
	"Stopped",
	"",
	"",
	"Stopped",
	"Stopped",
	"",
	"Cputime limit exceeded",
	"Filesize limit exceeded",
	"Virtual timer expired",
	"Profiling timer expired",
	"",
	"",
	"User defined signal 1",
	"User defined signal 2",
	NULL
};

t_sh				*new_h(char *name, char *path)
{
	t_sh				*new;
	char				*path_str;

	new = (t_sh *)malloc(sizeof(t_sh));
	path_str = (char *)malloc(ft_strlen(name) + ft_strlen(path) + 2);
	ft_strcpy(path_str, path);
	ft_strcat(path_str, "/");
	ft_strcat(path_str, name);
	if (new)
	{
		new->name = ft_strdup(name);
		new->path = path_str;
		new->next = NULL;
	}
	return (new);
}

void				push(t_sh **sh, char *name, char *path)
{
	t_sh				*new;
	t_sh				*temp;

	new = new_h(name, path);
	if (*sh == NULL)
		*sh = new;
	else
	{
		temp = *sh;
		while (temp->next && ft_strcmp(name, temp->next->name) > 0)
			temp = temp->next;
		new->next = temp->next;
		temp->next = new;
	}
}

void				ft_direct(t_shell **shell, char *path, char *arg)
{
	int					fd;

	if (access(path, F_OK) == 0)
	{
		if (access(path, X_OK) == 0)
		{
			chdir(path);
			save(shell);
		}
		else
		{
			ft_putstr_fd(arg, 2);
			if ((fd = open(path, O_DIRECTORY)) == -1)
				ft_putendl_fd(": not a directory", 2);
			else
				ft_putendl_fd(": permission denied", 2);
			if (fd > 0)
				close(fd);
		}
	}
	else
	{
		ft_putstr_fd("cd: no such file or directory: ", 2);
		ft_putendl_fd(arg, 2);
	}
}

void				get_stat(int i)
{
	int					error;

	if (!WIFSIGNALED(i))
		return ;
	error = WTERMSIG(i);
	if (error >= 0 && error < 32 && g_signals[error][0])
	{
		ft_putstr_fd("mini_shell_1: ", 2);
		ft_putendl_fd(g_signals[error], 2);
	}
}
