/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/26 18:34:12 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/01 14:25:22 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

void		process(char *prompt)
{
	if (lst->selected == 1 && lst->current == 1)
	{
		tputs(tgetstr("us", NULL), 1, tputchar);
		tputs(tgetstr("so", NULL), 1, tputchar);
		ft_putendl_fd(prompt, 2);
		tputs(tgetstr("se", NULL), 1, tputchar);
		tputs(tgetstr("ue", NULL), 1, tputchar);
	}
	else if (lst->current == 1 && lst->selected == 0)
	{
		tputs(tgetstr("us", NULL), 1, tputchar);
		ft_putendl_fd(prompt, 2);
		tputs(tgetstr("ue", NULL), 1, tputchar);
	}
	else if (lst->selected == 1 && lst->current == 0)
	{
		tputs(tgetstr("so", NULL), 1, tputchar);
		ft_putendl_fd(prompt, 2);
		tputs(tgetstr("se", NULL), 1, tputchar);
	}
	else
		ft_putendl_fd(prompt, 2);
}
/*
void		display(t_tab *lst, int color)
{
	t_tab		*temp;

	temp = NULL;
	process(lst);
	temp = lst->next;
	while (temp != lst)
	{
		display_check(temp, color);
		temp = temp->next;
	}
}
*/
