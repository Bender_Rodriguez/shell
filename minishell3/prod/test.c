# include "libft.h"
# include "colors.h"
# include <termcap.h>
# include <dirent.h>
# include <termios.h>
# include <curses.h>
# include <term.h>
# include <dirent.h>
# include <sys/wait.h>
# include <sys/ioctl.h>
# include <signal.h>

# define K_UP		(buf[0] == 27 && buf[1] == 91 && buf[2] == 65)
# define K_DOWN		(buf[0] == 27 && buf[1] == 91 && buf[2] == 66)
# define K_ENTER	(buf[0] == 10 && !buf[1] && !buf[2])
# define K_ECHAP	(buf[0] == 27 && !buf[1] && !buf[2])
# define K_SPACE	(buf[0] == 32 && !buf[1] && !buf[2])
# define K_DEL		(buf[0] == 127 && !buf[1] && !buf[2])
# define K_BAKSP	(buf[0] == 27 && buf[1] == 91 && buf[2] == 51)

int			main(void)
{
	char	buf[3];

	ft_memset(buf, 0, 3);
	read(0, buf, 3);
	while (!K_ECHAP)
	{
		printf("%c\n", buf[0]);
		printf("%c\n", buf[1]);
		printf("%c\n", buf[2]);
	}
	return (0);
}
