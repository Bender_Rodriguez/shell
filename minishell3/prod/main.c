/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/16 18:59:04 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/09 18:37:20 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

int			main(void)
{
	extern char			**environ;
	t_shell				*shell;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->hash = NULL;
	init_all(shell, environ);
	while (1)
	{
		prompt(shell);
	}
	termclose(shell);
	return (0);
}
