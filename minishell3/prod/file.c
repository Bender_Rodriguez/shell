/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/17 18:10:01 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/24 21:51:34 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sh3.h"

void		prompt(t_shell *shell)
{
	putcolor("MiniShell_3", BROWN, 1, 0);
	putcolor(" $> ", MAGENTA, 1, 0);
	if (get_next_line(0, &(shell->cmd)) == 0)
	{
		putcolor("Get_Next_Line ", MAGENTA, 2, 0);
		putcolor("STOP", BOLD_RED, 2, 1);
		exit(0);
	}
	else if (ft_strcmp("exit", shell->cmd) == 0)
	{
		termclose(shell);
		exit(0);
	}
	else
		putcolor(shell->cmd, BOLD_RED, 1, 1);
}
