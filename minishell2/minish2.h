/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minish2.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/24 23:27:24 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/15 12:01:46 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISH2_H
# define MINISH2_H

# include "libft.h"
# include "colors.h"
# include <dirent.h>
# include <sys/wait.h>
# include <signal.h>

# define IN				0
# define OUT			1
# define LEFT			0
# define RIGHT			1

typedef struct			s_sh
{
	char				*name;
	char				*path;
	int					time;
	struct s_sh			*next;
}						t_sh;
/*
typedef struct			s_pipe
{
	char				**cmds;
	char				**avs;
	char				**avs2;
	char				*path[2];
}						t_pipe;
*/
typedef struct			s_shell
{
	//t_pipe				*pipe;
	int					fd[2];
	int					pip;
	int					redir;
	int					pvirg;
	char				*filename;
	char				**env;
	char				*home;
	char				*path;
	char				*pwd;
	char				*old_pwd;
	t_sh				*sh;
	char				*cmd;
	char				*builtin[6];
}						t_shell;

void					double_fork(t_shell *shell, char **path, char **avs, char **avs2);
void					debug(char **av);
char					**get_avs_right(int i, char **arg);
char					**get_avs(int i, char **arg);
void					pipe_split(char **cmds, t_shell *shell);
void					pipe_fork(char **avs, char **avs2, char *path[2], t_shell *shell);
void					reset_pipe(t_shell *shell);
void					redir_in(t_shell *shell, char **avs, char **temp);
void					redir_out(t_shell *shell, char **avs, char **tempi, int two);
void					check_redir(t_shell *shell);
int						check_all(t_shell *shell);
char					**get_avs(int i, char **avs);
int						check_pipe(t_shell *shell);
void					ls_check(char **path);
void					get_stat(int i);
int						check_cmd(char *cmd, t_shell *shell);
void					cmd_split(t_shell *shell);
void					init_shell(t_shell **shell, char **environ);
void					ft_sh(t_shell **shell);
void					push(t_sh **sh, char *name, char *path);
t_sh					*new_h(char *name, char *path);
void					ft_shell(t_shell *shell);
char					*get_path(char *arg, t_sh *sh);
int						is_built(char *arg, t_shell *shell);
void					ft_fork(char *path, char **arg, t_shell *shell);
void					built(char **arg, t_shell *shell);
void					ft_cd(t_shell **shell, char **arg);
void					ft_env(t_shell *shell);
void					ft_direct(t_shell **shell, char *path, char *arg);
void					save(t_shell **shell);
void					ft_setenv(char *name, char *content, t_shell **shell);
void					new_env(t_shell **shell, char *str);
void					free_env(char ***env);
void					free_hash(t_sh **sh);
void					free_built(char **builtin);
void					ft_exit(t_shell **shell, char ***arg);
void					ft_checkenv(char **arg, t_shell **shell, int i);
void					ft_unsetenv(char **arg, t_shell **shell);
void					delete_env(t_shell **shell, char **arg, int i);

#endif
