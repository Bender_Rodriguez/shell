/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/06 23:57:31 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/13 19:22:39 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minish2.h"

char		*get_path(char *arg, t_sh *sh)
{
	if (access(arg, F_OK) == 0)
	{
		while (sh)
		{
			if (recurcmp(sh->path, arg) == 0)
				sh->time++;
			sh = sh->next;
		}
		return (arg);
	}
	else
	{
		while (sh)
		{
			if (recurcmp(sh->name, arg) == 0)
			{
				sh->time++;
				return (sh->path);
			}
			sh = sh->next;
		}
	}
	return (NULL);
}

int			is_built(char *arg, t_shell *shell)
{
	int		i;

	i = 0;
	while (shell->builtin[i])
	{
		if (ft_strcmp(arg, shell->builtin[i]) == 0)
			return (1);
		i++;
	}
	return (0);
}

void		ft_shell(t_shell *shell)
{
	char	**arg;
	char	*path;
	int		i;

	arg = ft_space_split(shell->cmd);
	if (arg && arg[0])
	{
		i = 0;
		if (is_built(arg[0], shell))
			built(arg, shell);
		else if ((path = get_path(arg[0], shell->sh)))
		{
			ls_check(&path);
			ft_fork(path, arg, shell);
		}
		else
		{
			putcolor("ft_minishell: command not found: ", BOLD_RED, 2, 0);
			putcolor(arg[0], BOLD_RED, 2, 1);
		}
		while (i < ft_tablen(arg))
			free(arg[i++]);
		free(arg);
	}
}

void		cmd_split(t_shell *shell)
{
	char	**temp;
	int		i;

	i = 0;
	/*
	putcolor("Redirection ", GREEN, 1, 0);
	putcolor(ft_itoa(shell->redir), GREEN, 1, 1);
	putcolor("Point virgule ", GREEN, 1, 0);
	putcolor(ft_itoa(shell->pvirg), GREEN, 1, 1);
	putcolor("Pipe ", GREEN, 1, 0);
	putcolor(ft_itoa(shell->pip), GREEN, 1, 1);
	*/
	if (shell->pvirg == 1 && shell->redir != 1 && shell->pip != 1)
	{
		putcolor("Point virgule detecte", GREEN, 1, 1);
		temp = ft_strsplit(shell->cmd, ';');
		while (temp[i])
		{
			free(shell->cmd);
			shell->cmd = NULL;
			shell->cmd = ft_strdup(temp[i]);
			ft_shell(shell);
			i++;
		}
	}
	else if (shell->redir == 1 && shell->pip != 1 && shell->pvirg != 1)
		check_redir(shell);
	else if (shell->redir == 1 && shell->pvirg == 1 && shell->pip != 1)
	{
		temp = ft_strsplit(shell->cmd, ';');
		shell->pvirg = 0;
		while (temp[i])
		{
			putcolor(temp[i], CYAN, 1, 1);
			i++;
		}
		i = 0;
		while (temp[i])
		{
			free(shell->cmd);
			shell->cmd = NULL;
			shell->cmd = ft_strdup(temp[i]);
			if (check_cmd(temp[i], shell))
				check_redir(shell);
			else
				ft_shell(shell);
			i++;
		}
	}
	else if (shell->redir != 1 && shell->pvirg != 1 && shell->pip == 1)
	{
		putcolor("Ca va pipe", GREEN, 1, 1);
		temp = ft_strsplit(shell->cmd, ' ');
		pipe_split(temp, shell);
	}
/*	{
		putcolor("Redirection detectee", GREEN, 1, 1);
		temp = ft_strsplit(shell->cmd, ' ');
		while (recurcmp(temp[i], ">") != 0)
			i++;
		avs = get_avs(i, temp);
		path = get_path(temp[0], shell->sh);
		shell->filename = ft_strdup(temp[i + 1]);
		shell->fd[OUT] = open(shell->filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
		i = dup(STDOUT_FILENO);
		dup2(shell->fd[OUT], STDOUT_FILENO);
		close(shell->fd[OUT]);
		ft_fork(path, avs, shell);
		dup2(i, STDOUT_FILENO);
		close(i);
	}
	*/
}

void		double_fork(t_shell *shell, char **path, char **avs, char **avs2)
{
	int		fd[2];
	pid_t	pid;

	pipe(fd);
	pid = fork();
	if (pid > 0)
	{
		close(fd[0]);
		dup2(fd[1], 1);
		close(fd[1]);
		execve(path[0], avs, shell->env);
		exit(0);
	}
	if (pid == 0)
	{
		close(fd[1]);
		dup2(fd[0], 0);
		close(fd[0]);
		execve(path[1], avs2, shell->env);
		exit(0);
	}
}

void		pipe_fork(char **avs, char **avs2, char **path, t_shell *shell)
{
	pid_t	pid;
	int		i;
	int		fd[2];

	i = 0;
	pipe(fd);
	pid = fork();
	if (pid == 0)
	{
		double_fork(shell, path, avs, avs2);
		exit(0);
	}
	if (pid > 0)
	{
		waitpid(pid, &i, 0);
		//get_stat(i);
		//wait(NULL);
	//	exit(1);
	}
}

void		ft_fork(char *path, char **arg, t_shell *shell)
{
	pid_t	pid;
	int		i;

	i = 0;
	pid = fork();
	if (pid == 0)
	{
		execve(path, arg, shell->env);
	//	exit (1);
	}
	if (pid > 0)
	{
		waitpid(pid, &i, 0);
		get_stat(i);
	}
}
