/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipesplit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/25 03:10:20 by yaitalla          #+#    #+#             */
/*   Updated: 2015/10/15 12:01:52 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minish2.h"

char			**get_avs_right(int i, char **arg)
{
	char		**ret;
	int			j;
	int			k;

	ret = (char **)malloc(sizeof(char *) * (ft_tablen(arg) - (i + 1)));
	j = 0;
	k = i + 1;
	i = ft_tablen(arg) - (i + 1);
	while (j < i)
	{
		ret[j] = ft_strdup(arg[k]);
		j++;
		k++;
	}
	ret[j] = NULL;
	return (ret);
}

void			pipe_split(char **cmds, t_shell *shell)
{
	int			i;
	char		**avs;
	char		**avs2;
	char		*path[2];
	
	i = 0;
	path[LEFT] = get_path(cmds[0], shell->sh);
//	debug(cmds);
	while (cmds[i][0] != '|')
		i++;
	avs = get_avs(i, cmds);
//	debug(avs);
	avs2 = get_avs_right(i, cmds);
	path[RIGHT] = get_path(cmds[i + 1], shell->sh);
	pipe_fork(avs, avs2, path, shell);
	shell->pip = 0;
}

void		debug(char **av)
{
	int		i = 0;

	while (av[i])
	{
		putcolor(av[i], CYAN, 1, 1);
		i++;
	}
}
