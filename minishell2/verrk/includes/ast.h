/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 16:32:32 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/28 12:45:04 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AST_H
# define AST_H

typedef enum		e_asttype
{
	A_SEMI,
	A_AND_IF,
	A_OR_IF,
	A_PIPE,
	A_CMD
}					t_asttype;

typedef enum		e_dir
{
	DIR_NONE,
	DIR_LEFT,
	DIR_RIGHT
}					t_dir;

typedef struct		s_ast
{
	struct s_cmd	*cmd;
	int				*pfd;
	t_asttype		type;
	int				fd;
	struct s_ast	*parent;
	struct s_ast	*left;
	struct s_ast	*right;
}					t_ast;

typedef enum		e_iotype
{
	IO_DEF,
	IO_PIPE,
	IO_TRUNC,
	IO_APPEND,
	IO_OUT,
	IO_READ
}					t_iotype;

typedef struct		s_cmdio
{
	t_iotype		type;
	char			*filename;
	int				fd;
	t_ast			*pipenode;
}					t_cmdio;

typedef struct		s_cmd
{
	char			*path;
	char			**argv;
	t_cmdio			fdin;
	t_cmdio			fdout;
}					t_cmd;

t_ast				*build_ast(t_token *token, t_ast *parent, t_dir dir);
t_ast				*build_ast_cmd(t_token *token, t_ast *parent, t_dir dir);
t_ast				*ast_create_node(t_ast *parent, t_dir dir, t_asttype type);
t_ast				*ast_create_cmd_node(t_ast *parent, t_dir dir);

#endif
