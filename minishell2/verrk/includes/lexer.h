/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 16:47:36 by verrk             #+#    #+#             */
/*   Updated: 2015/04/28 12:10:10 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEXER_H
# define LEXER_H

typedef enum		e_ttype
{
	SEMI,
	PIPE,
	DGREAT,
	GREAT,
	DLESS,
	LESS,
	AND_IF,
	OR_IF,
	WORD
}					t_ttype;

typedef struct		s_token
{
	char			*tok;
	t_ttype			type;
	struct s_token	*prev;
	struct s_token	*next;
}					t_token;

t_token				*get_token(char *line);
t_token				*lexer_find(t_token *token, t_ttype type);
t_token				*lexer_find_and_or(t_token *token);
t_token				*lexer_find_spec(t_token *token);
void				lexer_delone(t_token **token, t_token *tok);
int					lexer_size(t_token *token);
t_token				*lexer_split(t_token *tok);
int					lex_error(t_token *tok);
void				print_token(t_token *token);
void				free_token(t_token **list);

#endif
