/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 07:47:12 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/30 16:52:01 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SH_H
# define FT_SH_H

# include "libft.h"
# include "lexer.h"
# include "ast.h"
# include <sys/types.h>
# include <errno.h>
# include <dirent.h>
# include <sys/wait.h>
# include <signal.h>

typedef struct		s_hash
{
	char			*name;
	char			*path;
	int				time;
	struct s_hash	*next;
}					t_hash;

typedef struct		s_shell
{
	char			**env;
	char			*home;
	char			*path;
	char			*pwd;
	char			*old_pwd;
	t_hash			*hash;
	char			*cmd;
	char			*builtin[8];
	t_token			*token;
	t_ast			*ast;
	int				status;
}					t_shell;

void				ft_sh(t_shell *shell);
void				prompt(t_shell *shell);
void				ft_cmd(t_shell *shell, char *cmd);
int					is_pipe(char *cmd);
void				ft_pipe_redir(t_shell *shell, char *cmd, int i);
void				ft_redir_w(t_shell *shell, char *cmd, char *path);
void				push(t_hash **hash_table, char *name, char *path);
int					hash_find(t_hash *hash, char *name);
void				exit_shell(t_shell **shell, char **arg);
int					ft_checkenv(char **arg, t_shell **shell, int which);
int					ft_env(t_shell *shell, int fdout);
int					ft_setenv(char *name, char *value, t_shell **shell);
void				free_env(char ***env);
void				free_tab(char **tab);
int					ft_cd(t_shell **shell, char **arg);
int					ft_echo(char **arg, t_shell *shell, int fdout);
int					exec(t_ast *ast, t_shell *shell, int f);
int					exec_cmd(t_ast *ast, t_shell *shell, int f);
void				exec_pipe_child(t_ast *ast, t_shell *shell, int *ret);
void				exec_cmd_child(t_cmd *cmd, t_shell *shell, int fdin,
									int fdout);
int					isbuiltin(char *arg, t_shell *shell);
int					builtin(char **arg, t_shell *shell, int fdout, int f);
void				free_ast(t_ast *ast);

#endif
