/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 16:18:13 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/19 08:12:13 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

void			free_cmd(t_cmd *cmd)
{
	int			i;

	i = 0;
	while (cmd->argv[i])
		free(cmd->argv[i++]);
	free(cmd->argv);
	free(cmd->path);
	if (cmd->fdin.filename)
		free(cmd->fdin.filename);
	if (cmd->fdout.filename)
		free(cmd->fdout.filename);
	free(cmd);
}

void			free_ast(t_ast *ast)
{
	if (ast)
	{
		free_ast(ast->left);
		free_ast(ast->right);
		if (ast->cmd)
			free_cmd(ast->cmd);
		if (ast->pfd)
			free(ast->pfd);
		if (ast->fd != -1)
			close(ast->fd);
		free(ast);
	}
}

void			prompt(t_shell *shell)
{
	while (42)
	{
		ft_putstr(shell->pwd);
		ft_putstr("$> ");
		if (get_next_line(0, &shell->cmd) == 0)
			exit_shell(&shell, NULL);
		if (shell->cmd[0])
		{
			shell->token = get_token(shell->cmd);
			if (shell->token)
			{
				shell->ast = build_ast(shell->token, NULL, DIR_NONE);
				shell->status = exec(shell->ast, shell, 1);
				free_ast(shell->ast);
				shell->ast = NULL;
			}
		}
		free(shell->cmd);
	}
}
