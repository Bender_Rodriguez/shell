/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 16:32:17 by cpestour          #+#    #+#             */
/*   Updated: 2015/02/03 20:31:12 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

t_ast			*build_ast_semi(t_token *ltoken, t_token *tok,
								t_ast *parent, t_dir dir)
{
	t_ast		*node;
	t_token		*rtoken;
	int			first;

	first = 0;
	if (tok == ltoken)
		first = 1;
	node = ast_create_node(parent, dir, A_SEMI);
	rtoken = lexer_split(tok);
	if (!first)
		node->left = build_ast(ltoken, node, DIR_LEFT);
	else
		node->left = NULL;
	node->right = build_ast(rtoken, node, DIR_RIGHT);
	return (node);
}

t_ast			*build_ast_and_if(t_token *ltoken, t_token *tok,
								t_ast *parent, t_dir dir)
{
	t_ast		*node;
	t_token		*rtoken;

	node = ast_create_node(parent, dir, A_AND_IF);
	rtoken = lexer_split(tok);
	node->left = build_ast(ltoken, node, DIR_LEFT);
	node->right = build_ast(rtoken, node, DIR_RIGHT);
	return (node);
}

t_ast			*build_ast_or_if(t_token *ltoken, t_token *tok,
								t_ast *parent, t_dir dir)
{
	t_ast		*node;
	t_token		*rtoken;

	node = ast_create_node(parent, dir, A_OR_IF);
	rtoken = lexer_split(tok);
	node->left = build_ast(ltoken, node, DIR_LEFT);
	node->right = build_ast(rtoken, node, DIR_RIGHT);
	return (node);
}

t_ast			*build_ast_pipe(t_token *ltoken, t_token *tok,
								t_ast *parent, t_dir dir)
{
	t_ast		*node;
	t_token		*rtoken;

	node = ast_create_node(parent, dir, A_PIPE);
	rtoken = lexer_split(tok);
	node->left = build_ast(ltoken, node, DIR_LEFT);
	node->right = build_ast(rtoken, node, DIR_RIGHT);
	return (node);
}

t_ast			*build_ast(t_token *token, t_ast *parent, t_dir dir)
{
	t_token		*tok;

	if (token == NULL)
		return (NULL);
	tok = lexer_find(token, SEMI);
	if (tok)
		return (build_ast_semi(token, tok, parent, dir));
	tok = lexer_find_and_or(token);
	if (tok && tok->type == OR_IF)
		return (build_ast_or_if(token, tok, parent, dir));
	if (tok && tok->type == AND_IF)
		return (build_ast_and_if(token, tok, parent, dir));
	tok = lexer_find(token, PIPE);
	if (tok)
		return (build_ast_pipe(token, tok, parent, dir));
	return (build_ast_cmd(token, parent, dir));
}
