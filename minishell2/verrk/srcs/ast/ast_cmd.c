/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast_cmd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 18:33:05 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/28 12:33:32 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

static void		ast_cmd_pipe(t_ast *node)
{
	t_ast		*tmp;

	if (node->parent && node->parent->type == A_PIPE)
	{
		if (node->parent->left == node)
		{
			node->cmd->fdout.type = IO_PIPE;
			node->cmd->fdout.pipenode = node->parent;
		}
		if (node->parent->right == node)
		{
			node->cmd->fdin.type = IO_PIPE;
			node->cmd->fdin.pipenode = node->parent;
			tmp = node->parent;
			while (tmp->parent && tmp->parent->type == A_PIPE &&
					tmp->parent->right == tmp)
				tmp = tmp->parent;
			if (tmp->parent && tmp->parent->type == A_PIPE &&
				tmp->parent->left == tmp)
			{
				node->cmd->fdout.type = IO_PIPE;
				node->cmd->fdout.pipenode = tmp->parent;
			}
		}
	}
}

static void		ast_cmd_redir_out(t_ast *node, t_token **token)
{
	t_cmd		*cmd;
	t_token		*redir;

	cmd = node->cmd;
	redir = lexer_find_spec(*token);
	if (redir && (redir->type == GREAT || redir->type == DGREAT))
	{
		if (redir->type == GREAT)
			cmd->fdout.type = IO_TRUNC;
		if (redir->type == DGREAT)
			cmd->fdout.type = IO_APPEND;
		cmd->fdout.filename = ft_strdup(redir->next->tok);
		lexer_delone(token, redir->next);
		lexer_delone(token, redir);
	}
}

static void		ast_cmd_redir_in(t_ast *node, t_token **token)
{
	t_cmd		*cmd;
	t_token		*redir;

	cmd = node->cmd;
	redir = lexer_find_spec(*token);
	if (redir)
	{
		if (redir->type == LESS)
			cmd->fdin.type = IO_READ;
		cmd->fdin.filename = ft_strdup(redir->next->tok);
		lexer_delone(token, redir->next);
		lexer_delone(token, redir);
	}
}

static void		ast_cmd_arg(t_ast *node, t_token **token)
{
	t_cmd		*cmd;
	int			size;
	int			i;
	char		**argv;
	t_token		*tmp;

	cmd = node->cmd;
	tmp = *token;
	size = lexer_size(tmp);
	argv = (char **)malloc(sizeof(char *) * (size + 1));
	i = 0;
	while (i < size)
	{
		argv[i++] = ft_strdup(tmp->tok);
		tmp = tmp->next;
	}
	argv[i] = NULL;
	cmd->argv = argv;
}

t_ast			*build_ast_cmd(t_token *token, t_ast *parent, t_dir dir)
{
	t_ast		*node;

	node = ast_create_cmd_node(parent, dir);
	ast_cmd_pipe(node);
	ast_cmd_redir_out(node, &token);
	ast_cmd_redir_in(node, &token);
	ast_cmd_arg(node, &token);
	free_token(&token);
	return (node);
}
