/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 17:04:20 by verrk             #+#    #+#             */
/*   Updated: 2015/04/28 12:10:07 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

static void	new_token(t_token **list, char *tok, t_ttype type)
{
	t_token	*new;
	t_token	*tmp;

	new = (t_token *)malloc(sizeof(t_token));
	new->tok = tok;
	new->type = type;
	if (*list == NULL)
	{
		*list = new;
		new->prev = NULL;
	}
	else
	{
		tmp = *list;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
		new->prev = tmp;
	}
	new->next = NULL;
}

static int	is_spec(char c)
{
	return (c == ';' || c == '|' || c == '<' || c == '>' || c == '&');
}

static int	get_spec_token(t_token **list, char *line, int i)
{
	if (line[i + 1] == line[i])
	{
		if (line[i] == '<')
			new_token(list, ft_strsub(line, i, 2), DLESS);
		if (line[i] == '>')
			new_token(list, ft_strsub(line, i, 2), DGREAT);
		if (line[i] == '|')
			new_token(list, ft_strsub(line, i, 2), OR_IF);
		if (line[i] == '&')
			new_token(list, ft_strsub(line, i, 2), AND_IF);
		return (2);
	}
	else
	{
		if (line[i] == ';')
			new_token(list, ft_strsub(line, i, 1), SEMI);
		if (line[i] == '|')
			new_token(list, ft_strsub(line, i, 1), PIPE);
		if (line[i] == '<')
			new_token(list, ft_strsub(line, i, 1), LESS);
		if (line[i] == '>')
			new_token(list, ft_strsub(line, i, 1), GREAT);
		return (1);
	}
}

static int	get_cmd_token(t_token **list, char *line, int i)
{
	int		j;

	j = 0;
	while (line[i + j] && !is_spec(line[i + j]) && !ft_isspace(line[i + j]))
		j++;
	if (j > 0)
		new_token(list, ft_strsub(line, i, j), WORD);
	return (j);
}

t_token		*get_token(char *line)
{
	t_token	*token;
	int		i;

	token = NULL;
	i = 0;
	while (line[i])
	{
		while (ft_isspace(line[i]))
			i++;
		if (is_spec(line[i]))
			i += get_spec_token(&token, line, i);
		else
			i += get_cmd_token(&token, line, i);
	}
	if (lex_error(token))
	{
		free_token(&token);
		return (NULL);
	}
	return (token);
}
