/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 16:59:59 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/28 12:05:23 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

t_token			*lexer_find(t_token *token, t_ttype type)
{
	t_token		*tmp;
	t_token		*res;

	tmp = token;
	res = NULL;
	while (tmp)
	{
		if (tmp->type == type)
			res = tmp;
		tmp = tmp->next;
	}
	return (res);
}

t_token			*lexer_find_and_or(t_token *token)
{
	t_token		*tmp;
	t_token		*res;

	tmp = token;
	res = NULL;
	while (tmp)
	{
		if (tmp->type == OR_IF || tmp->type == AND_IF)
			res = tmp;
		tmp = tmp->next;
	}
	return (res);
}

t_token			*lexer_find_spec(t_token *token)
{
	t_token		*tmp;

	tmp = token;
	while (tmp)
	{
		if (tmp->type != WORD)
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

int				lexer_size(t_token *token)
{
	t_token		*tmp;
	int			i;

	tmp = token;
	i = 0;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

t_token			*lexer_split(t_token *tok)
{
	t_token		*rtoken;

	rtoken = tok->next;
	if (tok->prev)
		tok->prev->next = NULL;
	if (tok->next)
		tok->next->prev = NULL;
	free(tok->tok);
	free(tok);
	return (rtoken);
}
