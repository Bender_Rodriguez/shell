/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/03 20:42:28 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/28 12:12:36 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

static int	print_err(char *tok)
{
	ft_putstr_fd("Parse error near \'", 2);
	ft_putstr_fd(tok, 2);
	ft_putstr_fd("\'\n", 2);
	return (1);
}

int			lex_error(t_token *token)
{
	t_token	*tmp;

	tmp = token;
	while (tmp)
	{
		if (tmp->type != WORD && tmp->type != SEMI &&
			(!tmp->next || !tmp->prev ||
			tmp->next->type == SEMI || tmp->prev->type == SEMI))
			return (print_err(tmp->tok));
		if (tmp->type == DLESS)
			return (print_err(tmp->tok));
		tmp = tmp->next;
	}
	return (0);
}
