/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 18:20:51 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/20 15:31:15 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

int				ft_echo(char **arg, t_shell *shell, int fdout)
{
	int			i;
	int			n;

	n = 0;
	if (ft_strcmp(arg[1], "-n") == 0)
		n = 1;
	i = n + 1;
	while (arg[i])
	{
		if (i != n + 1)
			ft_putchar_fd(' ', fdout);
		if (ft_strcmp(arg[i], "$?") == 0)
			ft_putnbr_fd(shell->status, fdout);
		else
			ft_putstr_fd(arg[i], fdout);
		i++;
	}
	if (!n)
		ft_putchar_fd('\n', fdout);
	return (0);
}
