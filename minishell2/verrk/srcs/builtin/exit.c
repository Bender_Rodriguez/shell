/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/08 16:38:43 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/19 08:12:04 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh.h"

void			free_tab(char **tab)
{
	int			i;

	i = 0;
	while (i < ft_strlen_tab(tab))
		free(tab[i++]);
	free(tab);
}

void			free_env(char ***env)
{
	int			i;

	i = 0;
	while ((*env)[i])
	{
		free((*env)[i]);
		i++;
	}
	free(*env);
}

void			free_hash(t_hash **hash)
{
	t_hash		*tmp;

	while (*hash)
	{
		tmp = *hash;
		*hash = tmp->next;
		free(tmp->name);
		free(tmp->path);
		free(tmp);
	}
}

void			free_builtin(char **builtin)
{
	int			i;

	i = 0;
	while (builtin[i])
	{
		free(builtin[i]);
		i++;
	}
}

void			exit_shell(t_shell **shell, char **arg)
{
	int			status;

	status = (*shell)->status;
	if (arg && ft_strlen_tab(arg) >= 2)
		status = ft_atoi(arg[1]);
	free_env(&((*shell)->env));
	free((*shell)->path);
	free((*shell)->pwd);
	free((*shell)->old_pwd);
	free((*shell)->cmd);
	free((*shell)->home);
	free_hash(&((*shell)->hash));
	free_builtin((*shell)->builtin);
	if ((*shell)->ast)
		free_ast((*shell)->ast);
	free(*shell);
	exit(status);
}
