/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   child.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 14:43:12 by cpestour          #+#    #+#             */
/*   Updated: 2015/09/07 16:24:37 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_sh.h"

void			get_redir(t_ast *right)
{
	t_cmd		*tmp;

	tmp = right->cmd;
	if (tmp->fdout.type == IO_TRUNC)
	{
		tmp->fdout.fd = open(tmp->fdout.filename, O_CREAT | O_WRONLY |
							O_TRUNC, 0644);
		tmp->fdout.type = IO_OUT;
	}
	if (tmp->fdout.type == IO_APPEND)
	{
		tmp->fdout.fd = open(tmp->fdout.filename, O_CREAT | O_WRONLY |
							O_APPEND, 0644);
		tmp->fdout.type = IO_OUT;
	}
}

void			exec_pipe_child(t_ast *ast, t_shell *shell, int *ret)
{
	pid_t		pid;
	int			*pfd;

	get_redir(ast->right);
	pfd = (int *)malloc(sizeof(int) * 2);
	pipe(pfd);
	ast->pfd = pfd;
	pid = fork();
	if (pid == 0)
	{
		close(pfd[0]);
		exec(ast->left, shell, 0);
	}
	else
	{
		close(pfd[1]);
		wait(ret);
		exec_cmd(ast->right, shell, 0);
	}
}

void			exec_cmd_child(t_cmd *cmd, t_shell *shell, int fdin, int fdout)
{
	if (fdin != STDIN_FILENO)
		dup2(fdin, STDIN_FILENO);
	if (fdout != STDOUT_FILENO)
		dup2(fdout, STDOUT_FILENO);
	execve(cmd->path, cmd->argv, shell->env);
	exit(EXIT_FAILURE);
}
