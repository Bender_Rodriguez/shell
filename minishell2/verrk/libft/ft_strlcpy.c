/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 23:20:24 by verrk             #+#    #+#             */
/*   Updated: 2014/09/23 23:32:46 by verrk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		strlcpy(char *dst, const char *src, size_t size)
{
	size_t	src_len;

	size--;
	src_len = ft_strlen(src);
	if (src_len > size)
		src_len = size;
	ft_memcpy(dst, src, src_len);
	dst[src_len] = '\0';
	return (src_len);
}
