/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 15:00:08 by cpestour          #+#    #+#             */
/*   Updated: 2014/10/07 08:33:33 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_nbrlen(int n)
{
	size_t	i;

	i = 0;
	if (n < 0)
		n = -n;
	while (n)
	{
		n /= 10;
		i++;
	}
	return (i);
}
