/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/25 02:40:30 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/14 02:09:52 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minish2.h"

int			check_all(t_shell *shell)
{
	if (shell->redir == 1 && shell->pip == 1)
		return (1);
	return (0);
}

char		**get_avs(int i, char **arg)
{
	char	**ret;
	int		j;

	j = 0;
	ret = (char **)malloc(sizeof(char *) * i + 1);
	while (j < i)
	{
		ret[j] = ft_strdup(arg[j]);
		j++;
	}
	ret[j] = NULL;
	return (ret);
}

void		redir_in(t_shell *shell, char **avs, char **temp)
{
	//int		i;
	char	*path;

	path = get_path(temp[0], shell->sh);
	shell->fd[IN] = open(shell->filename, O_RDONLY);
	//i = dup(STDIN_FILENO);
	dup2(shell->fd[IN], STDIN_FILENO);
	close(shell->fd[IN]);
	ft_fork(path, avs, shell);
	ft_putchar('\n');
	//dup2(i, STDIN_FILENO);
	//close(i);
}

void		redir_out(t_shell *shell, char **avs, char **temp, int two)
{
	int		i;
	char	*path;

	path = get_path(temp[0], shell->sh);
	if (two == 0)
		shell->fd[OUT] = open(shell->filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	else
		shell->fd[OUT] = open(shell->filename, O_CREAT | O_WRONLY | O_APPEND, 0644);
	i = dup(STDOUT_FILENO);
	dup2(shell->fd[OUT], STDOUT_FILENO);
	close(shell->fd[OUT]);
	ft_fork(path, avs, shell);
	dup2(i, STDOUT_FILENO);
	close(i);
}

void		check_redir(t_shell *shell)
{
	char	**temp;
	char	**avs;
	int		i;

	i = 0;
	shell->redir = 0;
	putcolor("Redirection detectee", GREEN, 1, 1);
	temp = ft_strsplit(shell->cmd, ' ');
	while (temp[i] && temp[i][0] != '>' && temp[i][0] != '<')
		i++;
	putcolor(ft_itoa(ft_strlen(temp[i])), GREEN, 1, 1);
	shell->filename = ft_strdup(temp[i + 1]);
	avs = get_avs(i, temp);
	if (temp[i][0] == '>')
	{
		if (ft_strlen(temp[i]) > 1)
		{
			putcolor(">>", GREEN, 1, 1);
			redir_out(shell, avs, temp, 1);
		}
		else
		{
			putcolor(">", GREEN, 1, 1);
			redir_out(shell, avs, temp, 0);
		}
	}
	else if (temp[i][0] == '<')
	{
		putcolor("<", GREEN, 1, 1);
		redir_in(shell, avs, temp);
	}
	/*
	shell->fd[OUT] = open(shell->filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
	i = dup(fd);
	dup2(shell->fd[OUT], fd);
	close(shell->fd[OUT]);
	ft_fork(path, avs, shell);
	dup2(i, fd);
	close(i);
	*/
}
